import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import * as constants from './constants';

@Injectable()
export class CurrencyService {

    constructor(private http: Http) { }
    
        private getHeaders(): Headers {
            let headers = new Headers({
                'Content-Type': 'application/json',
            });
            return headers;
        }

        /*public subirPagos(params:FormData, posicion: number, biIdPago: any): Promise<any> {
            return this.http.post(
                constantes.urlSubirArchivo + '/cargar-pago/'+posicion+'/'+biIdPago,
                params
            ).toPromise()
            .then((data) => {return data;})
            .catch((error) => {throw error;})
        }*/

        public fnGetRate(parametro: any): Promise<any[]> {
            return this.http.get(
                constants.urlCurrency + '/CurrencyConverter',
            {   headers: this.getHeaders(), 
                params: parametro
            })
            .toPromise()
            .then((data) => {return data.json();})
            .catch((error) => {throw error;});
        }

        public fnGetCurrency(): Promise<any[]> {
            return this.http.get(
                constants.urlCurrency + '/Currency',
                {   headers: this.getHeaders(), 
                    params: {} 
                })
                .toPromise()
                .then((data) => {return data.json();})
                .catch((error) => {throw error;});
        }


         public fnLogin(User: any): Promise<any> {
            return this.http.post(
                constants.urlCurrency + '/User',  User,
                { headers: this.getHeaders() }
            ).toPromise()
            .then((data) => {return data;})
            .catch((error) => {throw error;})
        }
    

        validarCampos(campos: any[]): boolean {
            
            let correcto: boolean = true;
            for (let i = 0; i < campos.length; i++) {
                //if (campos[i].texto == '' || campos[i].texto == 0 || campos[i].texto == undefined) {
                    if (campos[i].texto == '' || campos[i].texto == undefined) {
                    //alert("Type to some value")
                    correcto = false;
                }
            }
    
            if (!correcto) {
                alert("Type to some value")

            }
    
            return correcto;
        }
}