import { Component, NgZone } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterService } from 'angular2-toaster';

@Component({
    selector: 'cambiar-clave-modal',
    templateUrl: './cambiar-clave-modal.component.html',
})
export class CambiarClaveModalComponent {

    public sistemas: any[];
    private biIdUsuario: any;
    public sistemaSeleccionado: any;
    public vcClaveActual: string;
    public vcClaveNueva: string;
    public vcClaveConfirmacion: string;
    public claveActualizada: boolean;
    bandera: boolean = true;

    constructor(
        private ngbActivemodal: NgbActiveModal, 
        private toaster: ToasterService){
        this.claveActualizada = false;
    }

    public cambiarClave(aceptarBtn: HTMLButtonElement): void{

        if(this.vcClaveNueva == null || this.vcClaveNueva.trim().length < 8){
            this.toaster.pop('error', 'La nueva contraseña debe poseer al menos 8 caracteres');
            return;
        }
        if(this.vcClaveNueva.indexOf(' ') != -1){
            this.toaster.pop('error', 'La nueva contraseña no puede tener espacios en blanco');
            return;
        }

        if(this.vcClaveConfirmacion != this.vcClaveNueva){
            this.toaster.pop('error', 'Confirme bien su nueva contraseña');
            return;
        }

        let vcUsuario = sessionStorage.getItem('vcUsuario');

        aceptarBtn.disabled = true;

        /*this.uService.cambiarClaveUsuarioAutenticado(vcUsuario, this.vcClaveActual, this.vcClaveNueva).then(() => {

            this.toaster.pop('success', 'Contraseña actualizada');

            sessionStorage.clear();

            this.ngbActivemodal.close();

        }).catch((e) => {
            console.log(e);
            if(e.mensaje != null && e.codigo == 40){
                this.toaster.pop('error', 'Su contraseña actual es incorrecta');
            }
            else{
                this.toaster.pop('error', 'Su contraseña actual es incorrecta');
                //this.toaster.pop('error', 'Ocurrió un error al cambiar su contraseña');
            }

            aceptarBtn.disabled = false;
        });*/
    }

    public cerrar(): void{
        this.ngbActivemodal.dismiss();
    }
}
