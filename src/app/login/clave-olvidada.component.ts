import { Component, OnInit } from '@angular/core';
//mport { UsuarioService } from '../servicios/seguridad/usuario.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
//import * as constantes from './../servicios/global/constantes';

@Component({
    selector: 'clave-olvidada',
    styleUrls: ['./login.scss'],
    templateUrl: './clave-olvidada.component.html'
})
export class ClaveOlvidadaComponent implements OnInit{

    fondo:any = {
        'background-image': 'url(./assets/img/fondo-login.jpg)',
        '-webkit-filter': 'brightness(40%)'
    }
    public vcClaveNueva: string;
    public vcClaveConfirmacion: string;
    public cambiandoClave: boolean;
    private encriptado: string;
    public claveActualizada: boolean;
    public bandera: boolean = false;
    
    constructor(
        //private uService: UsuarioService, 
        private aRoute: ActivatedRoute, 
        private toaster: ToasterService,
        private router: Router){
    }

    ngOnInit(){
        this.claveActualizada = false;
        this.cambiandoClave = false;

        this.aRoute.queryParamMap.subscribe((params) => {
            this.encriptado = params.get('vu');
        });
    }

    public cambiarClave(btnCambiar: HTMLButtonElement): void{

        if(this.vcClaveNueva == null || this.vcClaveNueva.trim().length == 0){
            this.toaster.pop('error', 'Error',  "Debe ingresar su nueva contraseña");
            return;
        }
        if(this.encriptado == null){
            this.toaster.pop('error', 'Error',  "Los datos han sido manipulados");
            return;
        }
        if( this.vcClaveNueva.indexOf(' ') != -1 || this.vcClaveNueva.length < 8){
            this.toaster.pop('error', 'Error',  "Las contraseñas debe tener al menos 8 caracteres");
            return;
        }
        if(this.vcClaveNueva != this.vcClaveConfirmacion){
            this.toaster.pop('error', 'Error',  "Las contraseñas no coinciden");
            return;
        }

        this.bandera = true;
        btnCambiar.disabled = true;

        /*this.uService.cambiarClavePorOlvido(this.encriptado, this.vcClaveNueva).then(() => {
            //this.claveActualizada = true;
            this.bandera = false;
            this.toaster.pop('success', 'Correcto', 'Su contraseña ha sido cambiada.');
            this.router.navigate(['login']);
        }).catch((e) => {
            this.bandera = false;
            btnCambiar.disabled = false;
            this.toaster.pop('error', 'Error', 'Ocurrió un error al cambiar la contraseña');
        });*/
    }
}