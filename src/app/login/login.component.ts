import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//import { UsuarioService } from '../servicios/seguridad/usuario.service';
import { CurrencyService } from '../servicios/converter/currency.service';
import { ToasterService } from 'angular2-toaster';
//import * as constantes from './../servicios/global/constantes';

@Component({
    selector: 'login-root',
    templateUrl: './login.html',
    styleUrls: ['./login.scss']
})
export class LoginComponent implements OnInit {

    fondo:any = {
        'background-image': 'url(./assets/img/full-screen-image-3.jpg)',
        '-webkit-filter': 'brightness(40%)'
    }
    typePassword: string = "password";
    showPassword: boolean = false;
    vcUsuario: string = "";
    vbClave: string = "";
    public sede: any;
    public sedes: any[];
    public usuarioIdentificado: boolean;
    private datosDeUsuario: any;
    public iniciandoSesion: boolean;
    public buscandoPermisos: boolean;
    bandera: boolean = false;
    
    constructor(
        private router: Router, 
        private currencyService: CurrencyService, 
        private toaster: ToasterService, ) {
        
    }

    ngOnInit() {
        this.sede = null;
        this.sedes = [];
        this.usuarioIdentificado = false;
        this.iniciandoSesion = false;
        this.buscandoPermisos = false;
    }

    logIn() {
        this.bandera = true;
        this.iniciandoSesion = true;
        var User:any={vcUser:this.vcUsuario, vcPassword:this.vbClave}
        //this.router.navigate(['modulos/converter/currency']);
        this.currencyService.fnLogin(User)
        .then((data) => {
            this.iniciandoSesion = false;
            this.bandera = false;   
            var OutputUser = JSON.parse(data._body);
            
            if (OutputUser.iIdUser != 0) {
                console.log(OutputUser)
                sessionStorage.setItem('iIdUser', OutputUser.iIdUser);
                sessionStorage.setItem('vcUser', OutputUser.vcUser);
                sessionStorage.setItem('vcNames', OutputUser.vcLastName+" "+OutputUser.vcName);
                this.router.navigate(['modulos/converter/currency']);
            }
            else {
                this.toaster.pop('error', 'Error',  'Usuario y/o contraseña incorrecta');
            }
        })
    }

    public cancelar(): void {
        this.buscandoPermisos = false;
        this.usuarioIdentificado = false;
        this.iniciandoSesion = false;
        this.datosDeUsuario = null;
    }

    mostrarContrasena() {
        this.showPassword = true;
        this.typePassword = "text";
    }

    ocultarContrasena() {
        this.showPassword = false;
        this.typePassword = "password";
    }
}