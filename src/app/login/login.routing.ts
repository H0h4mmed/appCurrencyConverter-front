import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { ClaveOlvidadaComponent } from './clave-olvidada.component';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'olvide-clave',
        component: ClaveOlvidadaComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);