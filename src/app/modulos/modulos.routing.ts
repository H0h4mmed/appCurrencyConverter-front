import { Routes, RouterModule } from '@angular/router';
import { ModulosComponent } from './modulos.component';
import { ModuleWithProviders } from '@angular/core';
import { GlobalInicioComponent } from './global/inicio/global-inicio.component';

export const routes: Routes = [
    {
        path: 'modulos',
        component: ModulosComponent,
        children: [
            { path: '', component: GlobalInicioComponent },
           // { path: 'pagos', loadChildren: './pagos/pagos.module#PagosModule' },
            { path: 'converter', loadChildren: './converter/converter.module#ConverterModule' },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);