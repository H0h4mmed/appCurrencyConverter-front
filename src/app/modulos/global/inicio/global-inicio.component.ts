import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterService } from 'angular2-toaster';

@Component({
    selector: 'global-inicio',
    templateUrl: './global-inicio.component.html',
    styleUrls: ['./global-inicio.component.scss']
})

export class GlobalInicioComponent {

    cantidad: number;
    nombreUsuario: string;
    bandera: boolean = true;
    public permisos: any;

    constructor(
        private router: Router,
        private toasterService: ToasterService,
    ) { }

    ngOnInit() {
        this.nombreUsuario = sessionStorage.getItem('vcName');
    }
}
