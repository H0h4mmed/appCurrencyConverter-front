import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'modulo-converter',
    template: '<router-outlet></router-outlet>'
})
export class ConverterComponent {

    constructor() { }

}