import { Route, RouterModule } from '@angular/router';
import { ConverterComponent } from './converter.component';
import { ModuleWithProviders } from '@angular/core';
import { CurrencyComponent } from './currency/currency.component';


let routes: Route[] = [
    {
        path: '', component: ConverterComponent, children: [ {path: 'currency', component: CurrencyComponent},
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);