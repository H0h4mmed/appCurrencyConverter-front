import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterService } from 'angular2-toaster';
import * as $ from 'jquery';
import {Observable} from 'rxjs/Rx';
import { DecimalPipe } from '@angular/common'
import { CurrencyService } from './../../../servicios/converter/currency.service';

@Component({
    selector: 'converter-currency',
    templateUrl: './currency.component.html',
    styleUrls: ['./currency.component.scss']
})

export class CurrencyComponent {

        bandera:boolean = false;
        banderaContent: boolean = true;
        currencys: any[] = []; 

        //Se define los valores por default
        converter:any={
            //nuValueInput:0.0000,
            vcBase:'USD',  dDate: Date,  vcRate:'EUR', nuValue:0.0000, disabled: true,
            vcSymbol:'EUR'
        }

        query: any={vcBase:"",vcSymbol:""  }

        OutputConverter: any = {};


        /* CALENDAR */
        start: any = new Date(new Date().setDate(new Date().getDate() - 30)).toISOString().slice(0,10);
        end: any = new Date().toISOString().slice(0,10);
    
        public daterange: any = {
            start: this.start, 
            end: this.end,
            startFormat: this.start.split("-")[2]+'/'+this.start.split("-")[1]+'/'+this.start.split("-")[0], 
            endFormat: this.end.split("-")[2]+'/'+this.end.split("-")[1]+'/'+this.end.split("-")[0], 
        };
    
        public options: any = {
            "alwaysShowCalendars": "false",
            "maxDate": new Date(),
            "startDate": this.daterange.startFormat,
            "endDate": this.daterange.endFormat,
            "opens": "right",
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aceptar",
                "cancelLabel": "Cancelar",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
        };
        /* END CALENDAR */

    constructor(
        private router: Router,
        private modalService: NgbModal,
        private toasterService: ToasterService,
        private currencyService: CurrencyService,
  
    ) { }

    ngOnInit() {
        if(sessionStorage.getItem('iIdUser') == null ||
        sessionStorage.getItem('vcUser') == null ||
        sessionStorage.getItem('vcNames') == null){
            this.toasterService.pop('error',"Error", 'Usted no tiene permisos para acceder a esta página');
            sessionStorage.clear();
            this.router.navigate(['/login']);
        }else{
            this.banderaContent = true;
            this.currencyService.fnGetCurrency().then((data) => {
            this.currencys = data;
            });
          
       }             
    }

       /* ------ CALENDAR ------ */    
       public selectedDate(value: any) { 
        this.daterange.start = value.start._d.toISOString().slice(0,10);
        this.daterange.end = value.end._d.toISOString().slice(0,10);
        
    }

    public focus(){
        $(".daterangepicker.dropdown-menu").css("visibility", "visible");
        $(".daterangepicker.dropdown-menu").css("opacity", "1");
    }
    /* ------ END CALENDAR ------ */
    onSubmit(){
        let camposVaciosValidar = [
            { texto: this.converter.nuInputValue, error: 'Input Some text' },
        ];

        if (this.currencyService.validarCampos(camposVaciosValidar)) { 
            this.Calculate();
            }
    }

    Calculate()
    {   
        this.query.vcBase = this.converter.vcBase
        this.query.vcSymbol = this.converter.vcSymbol;
        this.currencyService.fnGetRate(this.query).then((data) => {            
            this.OutputConverter= data       
            this.converter.nuOutputValue=this.converter.nuInputValue* this.OutputConverter.nuValue
            //this.bancos = data;   cc
        }).catch((error) => {
            this.toasterService.pop('error', 'Error', 'Ha ocurrido un error: ' + error);
        })
        ;
        
    }
}