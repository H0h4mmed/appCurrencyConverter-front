import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './converter.routing';
import { SelectModule } from 'ng2-select';
import { NouisliderModule } from 'ng2-nouislider';
import { Ng2CompleterModule } from "ng2-completer";
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { Daterangepicker } from 'ng2-daterangepicker';

//SERVICIOS

//COMPONENTES
import { SpinerComponent } from './recursos/spinner/spinner.component';
import { ConverterComponent } from './converter.component';
import { CurrencyComponent } from './currency/currency.component';


//MODALS

@NgModule({
    imports: [
        HttpModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        SelectModule,
        NouisliderModule,
        Ng2CompleterModule,
        ConfirmationPopoverModule.forRoot({
            confirmButtonType: 'danger'
        }),
        Daterangepicker,
    ],
    declarations: [
        SpinerComponent,
        ConverterComponent,
        CurrencyComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ]
})

export class ConverterModule { }