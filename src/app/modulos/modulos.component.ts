import { Component, OnInit, OnDestroy, HostListener  } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
    selector: 'modulos-root',
    templateUrl: './modulos.html',
    styleUrls: ['./modulos.scss']
})
export class ModulosComponent implements OnInit {
    
    public tieneAccesoAlERP: boolean;
    constructor(private router: Router,) { 
    }

    ngOnInit() {

        this.tieneAccesoAlERP = true;

        /*
        this.tieneAccesoAlERP = false;

        let biIdUsuarioSistemaSede = Number(sessionStorage.getItem('biIdUsuarioSistemaSede'));

        this.ussService.obtenerFamiliaDePermisos(biIdUsuarioSistemaSede, 'erp-modulos').then((permisos) => {
            
            if(permisos == null || !(this.caService.verificarOperacion(permisos, 'VISIBLE') && 
                this.caService.verificarOperacion(permisos, 'ENABLE'))){
                console.log('NO TIENE PERMISO');
                this.router.navigate(['/login']);
            }
            else{
                this.tieneAccesoAlERP = true;
            }
        }).catch((e) => {
            console.log(e);
            sessionStorage.clear();
            this.router.navigate(['/login']);
        });*/
    }

    /*@HostListener('window:beforeunload') onBeforeUnload() {
        console.log(window.event)
        console.log("a", window.event.cancelable)
        console.log("b", $(window).width())
        console.log("c", window.event.keyCode);
        var confirmClose = confirm('Close?');
        return confirmClose;
    }*/

    @HostListener('window:beforeunload', ['$event'])
    onBeforeUnload($event: any) {
        this.cerrarSesion();
    }
  

    cerrarSesion(){
        let token = sessionStorage.getItem('jwt');
        let biIdUsuarioSistema = sessionStorage.getItem('biIdUsuarioSistema');
        /*this.personaService.cerrarSesion(token, biIdUsuarioSistema).then((data)=> {
            //sessionStorage.clear();
        });*/
    }

}
