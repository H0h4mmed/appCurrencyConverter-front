import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


declare const $: any;

export let ROUTES: any[];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];
    token: string;
    //urlApp:string = constantes.urlApp;
    private sidebarPlantilla: any[];
    private permisosMenuRRHH: any;
    private permisosMenuConfiguracion: any;
    private permisosSeguridad: any;
    private biIdUsuarioSistemaSede: number;

    constructor(
        private router: Router,
    ) { }

    ngOnInit() {
        ROUTES = [];
        this.biIdUsuarioSistemaSede = Number(sessionStorage.getItem('biIdUsuarioSistemaSede'));
        /*this.usuarioSistemaService.generarTokenUsuarioSistemaSede(this.biIdUsuarioSistemaSede)
        .then((data:any)=>{
            this.token = data.token;
            this.dibujarPanel();
        });*/
        this.dibujarPanel();
    }

    dibujarPanel(){
        this.sidebarPlantilla = [/*{
            path: 'pagos', title: 'Solicitud Cita', icon: 'fa fa-book', vcObjeto: 'modulo-pagos', children: [
                { path: '/modulos/pagos/cita-pendiente', title: 'Listado de Citas', vcObjeto: 'cita-pendiente-listar', enabled: true },
                { path: '/modulos/pagos/pago-cita', title: 'Listado de Pagos', vcObjeto: 'pago-cita-listar', enabled: true },
            ]},*/{
            path: 'converter', title: 'Curency Converter', icon: 'fa fa-dollar', vcObjeto: 'modulo-converter', children: [
                { path: '/modulos/converter/currency', title: 'Currency', vcObjeto: 'currency-converter-listar', enabled: true },
            ]}
        ];
        
        for (let m = 0; m < this.sidebarPlantilla.length; ++m) {
            this.generarSidebar(this.sidebarPlantilla[m], ROUTES);
        }

        this.menuItems = ROUTES.filter(menuItem => menuItem);


        /*this.sidebarService.obtenerSidebar(Number(sessionStorage.getItem("biIdUsuarioSistemaSede"))).then((sidebar) => {
            for (let m = 0; m < this.sidebarPlantilla.length; ++m) {
                this.anularMenus(this.sidebarPlantilla[m], sidebar);
            }

            for (let m = 0; m < this.sidebarPlantilla.length; ++m) {
                this.generarSidebar(this.sidebarPlantilla[m], ROUTES);
            }

            this.menuItems = ROUTES.filter(menuItem => menuItem);
        });*/
    }

    private anularMenus(menu, menusUsuario) {
        if (menusUsuario[menu.vcObjeto] == null || menusUsuario[menu.vcObjeto].VISIBLE == null) {
            menu.deleted = true;
            return;
        }

        menu.enabled = (menusUsuario[menu.vcObjeto].ENABLE != null);

        if (menu.children != null) {
            for (let h = 0; h < menu.children.length; ++h) {
                this.anularMenus(menu.children[h], menusUsuario);
            }
        }
    }

    private generarSidebar(menu, sidebar: any[]) {

        if (menu.deleted == null) {
            let menuNuevo = {
                path: menu.path,
                title: menu.title,
                icon: menu.icon,
                url: menu.url,
                enabled: menu.enabled
            };

            sidebar.push(menuNuevo);

            if (menu.children != null) {
                menuNuevo['children'] = [];

                for (let h = 0; h < menu.children.length; ++h) {
                    this.generarSidebar(menu.children[h], menuNuevo['children']);
                }
            }
        }
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    }

    mandaInicio() {
        this.router.navigate(['/modulos/converter/currency']);
    }

    
}