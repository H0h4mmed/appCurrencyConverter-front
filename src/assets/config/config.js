var SERVICIOS  = {
    URL:'http://localhost', //Ejemplo: 'http://172.16.1.220'
    PUERTO:'', //Ejemplo: '12000'
    APLICATIVO:'',//Ejemplo: 'ERP_ADMIN'
}

var SERVER  = {
    URL:'http://172.16.57.40', //Ejemplo: 'http://172.16.1.220'
    PUERTO:'4201', //Ejemplo: '12000'
    APLICATIVO:'', //Ejemplo: 'ERP_ADMIN'
    DIRECTORIO:'' //Ejemplo: 'erp-fron'
}

var IIDSISTEMA = 4;
var RUTA_TCN = 'http://localhost:4200' //http://172.16.1.220:12000/TrabajaConNosotros/Views';

var ENTORNOS = {
    LOCAL: 0,
    DESARROLLO: 1,
    TEST: 2,
    PRODUCCION: 3
};

var REPOSITORIO = {
    LR_ACTA_COMISION: "LR_ACTA_COMISION", //10
    LR_ANTICIPADA: "LR_ANTICIPADA", //11
    LR_CARTA: "LR_CARTA", //12
    LR_CONFORMIDAD_CLIENTE: "LR_CONFORMIDAD_CLIENTE", //13
    LR_FISICOS: "LR_FISICOS", //14
    LR_RESPUESTA_CLIENTE: "LR_RESPUESTA_CLIENTE", //15
};

var CORREO_RESPUESTA_CLIENTE_INDECOPI = {
    subject: "Respuesta de Solución sobre el Libro de Reclamación",
    text: "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='background-color:#f8f8f8;font-family: 'Open Sans','Helvetica','Arial',sans-serif;' height='100%'><tbody><tr><td valign='top' style='padding-top:20px;padding-bottom:20px' align='center'><table cellpadding='0' cellspacing='0' width='700' style='border:1px solid #f2f2f2;border-radius:5px;'><tbody><tr><td valign='top' style='background-color:#fff;border-bottom:1px solid #f2f2f2;'><table border='0' cellpadding='0' cellspacing='0' width='100%' style=' '><tbody><tr><td valign='middle' align='left' style='text-align:center;'><a style='padding:10px' href='http://www.natclar.com.pe' target='_blank'><img src='https://ci6.googleusercontent.com/proxy/9m0dL-Kyddn5pkg2Hk2CjKhtTpDbw2M_RIpZ_yklIdT16z03_Q1TtGFlmuqNE0lKmOP27AsEg7nw2n1qyOTXAg07gYBqrHZ4stp1HFB8Myhw3AAz=s0-d-e1-ft#https://www.natclar.com.pe/wp-content/uploads/2016/12/logo.png' width='150' height='80' alt=''></a></td></tr></tbody></table></td></tr><tr style='background-color:#ffffff'><td valign='top' style='padding:10px 5px 10px 5px'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'><tbody><tr><td width='23'>&nbsp;</td><td><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' style='border-collapse:collapse'><tbody><tr><td style='font-weight:300;font-size: 16px;font-family:Open Sans,Arial,Helvetica,sans-serif;color:#333332;'>Hola, [@vcNombres]:</td></tr><tr><td height='20'>&nbsp;</td></tr></tbody></table></td><td width='23' >&nbsp;</td></tr></tbody></table><table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'><tbody><tr><td width='23' >&nbsp;</td><td><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' ><tbody><tr><td style='font-weight:300;font-size: 16px;font-family:Open Sans,Arial,Helvetica,sans-serif;color:#333332;'>[@nvDetalleCorreo]</td><tr><td height='20'>&nbsp;</td><td height='20'>&nbsp;</td></tr><tr><td style='font-weight:300;font-size: 16px;font-family:Open Sans,Arial,Helvetica,sans-serif;color:#333332;'>A la espera de su conformidad</td><tr><td height='20'>&nbsp;</td><td height='20'>&nbsp;</td></tr><tr><td height='20' style='font-weight:300;font-size:16px;font-family:Open Sans,Arial,Helvetica,sans-serif;color:#333332'>Saludos</td><td height='20'>&nbsp;</td></tr></tbody></table><table width='100%' border='0' cellspacing='0' cellpadding='0' align='right' style='margin-top:20px'><tbody><tr><td style='font:14px/19px Arial,Helvetica,sans-serif;color:#555'>Si necesita ayuda, por favor póngase en contacto con nosotros, al correo: <b> <a href='mailto:sac@natclar.com.pe' target='_blank'>sac@natclar.com.pe</a>. </b></td></tr><tr><td height='20'>&nbsp;</td></tr><tr><td height='20'>&nbsp;</td></tr></tbody></table></td></tr></tbody></table></td><td width='23'>&nbsp;</td></tr><tr><td height='16'>&nbsp;</td><td height='16'>&nbsp;</td><td height='16'>&nbsp;</td></tr></tbody></table><table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-top:1px solid #f2f2f2;padding-top:15px'><tbody><tr><td valign='middle' align='center'><span style='font-size:12px;color:#3e3e3e'>Copyrights © 2018 Natclar | Todos los Derechos Reservados</span></td></tr></tbody></table></td></tr></tbody></table>",
    subText: "Mediante la presente se envía Carta de Respuesta referente a reclamo presentado en Libro de Reclamaciones ([@tipo]) N°[@numero] - [@sede]"
}

var CORREO_RESPUESTA_CLIENTE_SUSALUD = {
    subject: "Respuesta de Solución sobre el Libro de Reclamación",
    text: "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='background-color:#f8f8f8;font-family: 'Open Sans','Helvetica','Arial',sans-serif;' height='100%'><tbody><tr><td valign='top' style='padding-top:20px;padding-bottom:20px' align='center'><table cellpadding='0' cellspacing='0' width='700' style='border:1px solid #f2f2f2;border-radius:5px;'><tbody><tr><td valign='top' style='background-color:#fff;border-bottom:1px solid #f2f2f2;'><table border='0' cellpadding='0' cellspacing='0' width='100%' style=' '><tbody><tr><td valign='middle' align='left' style='text-align:center;'><a style='padding:10px' href='http://www.natclar.com.pe' target='_blank'><img src='https://ci6.googleusercontent.com/proxy/9m0dL-Kyddn5pkg2Hk2CjKhtTpDbw2M_RIpZ_yklIdT16z03_Q1TtGFlmuqNE0lKmOP27AsEg7nw2n1qyOTXAg07gYBqrHZ4stp1HFB8Myhw3AAz=s0-d-e1-ft#https://www.natclar.com.pe/wp-content/uploads/2016/12/logo.png' width='150' height='80' alt=''></a></td></tr></tbody></table></td></tr><tr style='background-color:#ffffff'><td valign='top' style='padding:10px 5px 10px 5px'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'><tbody><tr><td width='23'>&nbsp;</td><td><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' style='border-collapse:collapse'><tbody><tr><td style='font-weight:300;font-size: 16px;font-family:Open Sans,Arial,Helvetica,sans-serif;color:#333332;'>Hola, [@vcNombres]:</td></tr><tr><td height='20'>&nbsp;</td></tr></tbody></table></td><td width='23' >&nbsp;</td></tr></tbody></table><table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'><tbody><tr><td width='23' >&nbsp;</td><td><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' ><tbody><tr><td style='font-weight:300;font-size: 16px;font-family:Open Sans,Arial,Helvetica,sans-serif;color:#333332;'>[@nvDetalleCorreo]</td><tr><td height='20'>&nbsp;</td><td height='20'>&nbsp;</td></tr><tr><td height='20' style='font-weight:300;font-size:16px;font-family:Open Sans,Arial,Helvetica,sans-serif;color:#333332'>Saludos</td><td height='20'>&nbsp;</td></tr></tbody></table><table width='100%' border='0' cellspacing='0' cellpadding='0' align='right' style='margin-top:20px'><tbody><tr><td style='font:14px/19px Arial,Helvetica,sans-serif;color:#555'>Si necesita ayuda, por favor póngase en contacto con nosotros, al correo: <b> <a href='mailto:sac@natclar.com.pe' target='_blank'>sac@natclar.com.pe</a>. </b></td></tr><tr><td height='20'>&nbsp;</td></tr><tr><td height='20'>&nbsp;</td></tr></tbody></table></td></tr></tbody></table></td><td width='23'>&nbsp;</td></tr><tr><td height='16'>&nbsp;</td><td height='16'>&nbsp;</td><td height='16'>&nbsp;</td></tr></tbody></table><table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-top:1px solid #f2f2f2;padding-top:15px'><tbody><tr><td valign='middle' align='center'><span style='font-size:12px;color:#3e3e3e'>Copyrights © 2018 Natclar | Todos los Derechos Reservados</span></td></tr></tbody></table></td></tr></tbody></table>",
}

var ENTORNO = ENTORNOS.LOCAL;